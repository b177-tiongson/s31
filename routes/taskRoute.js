const express = require("express")

//create a Router instance that functions as a routing system
const router = express.Router()

//import the taskControllers
const taskController = require("../controllers/taskControllers")

// route to get all the task
router.get("/",(req,res)=>{
	taskController.getAllTasks()
	.then((result)=>res.send(result))
})

//route to create a task
router.post("/",(req,res)=>{
	taskController.createTask(req.body)
	.then((result)=>res.send(result))
})

//route to delete a task
router.delete("/:id",(req,res)=>{
	taskController.deleteTask(req.params.id)
	.then((result)=>res.send(result))

})

//route to update a task
router.put("/:id",(req,res)=>{
	taskController.updateTask(req.params.id, req.body)
	.then((result)=>res.send(result))
})


//Create a route for getting a specific task.

router.get("/:id",(req,res)=>{
	taskController.getSpecificTask(req.params.id, req.body)
	.then((result)=>res.send(result))
})

router.put("/:id/complete",(req,res)=>{
	taskController.updateTaskStatus(req.params.id, req.body)
	.then((result)=>res.send(result))
})


//export the router object to be used in index.js
module.exports = router