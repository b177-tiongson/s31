const Task = require("../models/task")

//Controller functions for getting all tasks
module.exports.getAllTasks = () =>{
	return Task.find({}).then(result => {

		return result

	})
}

//controller funtion for creating a task
module.exports.createTask = (requestBody)=>{
	//create a task object based on the mongoose model "task"

	let newTask = new Task({
		//set the "name" property with the value received from the client
		name : requestBody.name
	})

	return newTask.save().then((task, error)=> {
		if(error){
			console.log(error)
			return false
		}
		else{
			return task
		}

	})

}

//controller for delete task
module.exports.deleteTask = (taskId)=>{

	return Task.findByIdAndRemove(taskId)
	.then((removedTask,err)=>{
		if(err){
			console.log(err)
			return false
		}
		else{
			return removedTask
		}
	})

}

//controller function for updating a task
module.exports.updateTask = (taskId, newContent)=>{

	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error)
			return false
		}
		
		result.name = newContent.name

		//saves the updated result in the mongoDB database

		return result.save().then((updatedTask,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			}
			else{

				return updatedTask
			}
		})

	})

}


//----------------------ACTIVITY------------------


//Create a controller function for retrieving a specific task.

module.exports.getSpecificTask = (taskId) =>{
	return Task.findById(taskId).then((result,err) => {

		if(err){
			console.log(err)
			return false
		}
		else{

			return result
		}
	
	})
}

//Create a route for changing the status of a task to "complete".

module.exports.updateTaskStatus = (taskId)=>{

	return Task.findById(taskId).then((result,error)=>{
		if(error){
			console.log(error)
			return false
		}
		
		result.status = "complete"

		//saves the updated result in the mongoDB database

		return result.save().then((updatedTaskStatus,saveErr)=>{
			if(saveErr){
				console.log(saveErr)
				return false
			}
			else{

				return updatedTaskStatus
			}
		})

	})

}