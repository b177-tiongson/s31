const mongoose = require("mongoose")

//create the schema and model

const taskSchema = new mongoose.Schema({
	name: String,
	status:{
		type: String,
		default: "pending"
	}
})


//export the file
module.exports = mongoose.model("Task", taskSchema)